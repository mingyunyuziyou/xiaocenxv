<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2021/2/24
 * Time: 下午3:19
 */

namespace app\lib\exception;


class SuccessMessage
{
    public $code = 201;
    public $msg = 'ok';
    public $errorCode = 0;
}
