<?php


namespace app\lib\exception;


use think\Exception;

class ParameterException extends BaseException
{
    //HTTP 状态吗 404，200
    public $code = 400;

    //错误代码
    public $msg = '参数错误';

    //自定义的错误码
    public $errorCode = 10000;
    

}
