<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/11/2
 * Time: 下午11:36
 */

namespace app\lib\exception;


class CategoryException extends BaseException
{
    public $code = 404;
    public $msg = '指定类目不存在，请检查参数';
    public $errorCode = 50000;

}
