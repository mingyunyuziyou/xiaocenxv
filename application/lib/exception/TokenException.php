<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/12/9
 * Time: 上午12:21
 */

namespace app\lib\exception;


class TokenException extends BaseException
{
    public $code = 401;
    public $msg = 'Token已过期或无效';
    public $errorCode = 10001;
}
