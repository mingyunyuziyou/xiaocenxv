<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/12/8
 * Time: 下午9:57
 */

namespace app\lib\exception;


class WeChatException extends BaseException
{
    public $code = 400;
    public $msg = '微信服务接口调用失败';
    public $errorCode = 999;

}
