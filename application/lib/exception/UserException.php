<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2021/2/24
 * Time: 下午3:02
 */

namespace app\lib\exception;


class UserException extends BaseException
{
    public $code = 404;
    public $msg = '用户不存在';
    public $errorCode = 60000;

}
