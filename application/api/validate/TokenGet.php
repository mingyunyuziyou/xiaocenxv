<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/12/2
 * Time: 下午8:56
 */

namespace app\api\validate;


class TokenGet extends BaseValidate
{
    protected $rule = [
        'code' => 'require|isNotEmpty'
    ];


    protected $message = [
        'code' => '没有code还想获取token,做梦...'
    ];


}
