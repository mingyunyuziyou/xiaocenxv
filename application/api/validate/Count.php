<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/10/31
 * Time: 下午10:36
 */

namespace app\api\validate;


class Count extends BaseValidate
{
    protected $rule = [
        'count' => 'isPositiveInteger|between:1,15'
    ];

}
