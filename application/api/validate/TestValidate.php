<?php


namespace app\api\validate;


use think\Validate;

class TestValidate extends Validate
{
    protected $rule = [
        'name|名称' => 'require|max:10',
        'email|邮箱' => 'email'
    ];

}
