<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/12/12
 * Time: 下午11:28
 */

namespace app\api\model;


class ProductImage extends BaseModel
{
    protected $hidden = ['img_id', 'delete_time', 'product_id'];

    public function imgUrl()
    {
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}
