<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/11/2
 * Time: 下午11:24
 */

namespace app\api\model;


class Category extends BaseModel
{
    protected $hidden = ['create_time', 'delete_time', 'update_time'];

    public function img()
    {
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

}
