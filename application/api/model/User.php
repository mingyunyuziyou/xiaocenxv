<?php
/**
 * Created by PhpStorm
 * User: kun
 * Date: 2020/12/2
 * Time: 下午9:06
 */

namespace app\api\model;


class User extends BaseModel
{
    public function address()
    {
        return $this->hasOne('UserAddress', 'user_id', 'id');
    }

    public static function getByOpenID($openid)
    {
        $user = self::where('openid', '=', $openid)->find();
        return $user;
    }


}
