<?php


namespace app\api\model;


use app\api\model\Banner as BannerModel;
use think\Db;
use think\Exception;
use think\Model;

class Banner extends BaseModel
{
    protected $hidden = ['delete_time', 'update_time'];

    //一对多
    //关联模型名 关联模型的外键 当前模型关联的键明
    public function items()
    {
        return $this->hasMany('BannerItem', 'banner_id', 'id');
    }


    public static function getBannerByID($id)
    {
        //Db 查询
//        $result = Db::query("select * from banner_item where banner_id={$id}");
//        $result = Db::table('banner_item')
//            ->where(function ($query) use ($id) {
//                $query->where('banner_id', '=', $id);
//            });
//        $result = Db::table('banner_item')
//            ->where('banner_id', $id)
//            ->select();

        $banner = self::with(['items', 'items.img'])->find($id);
        return $banner;
    }

}
