<?php

namespace app\api\model;

use think\Model;

class BannerItem extends BaseModel
{
    protected $hidden = ['delete_time', 'update_time', 'id', 'img_id', 'banner_id'];

    //一对一关联模型 belongsTo
    public function img()
    {
        return $this->belongsTo('Image', 'img_id', 'id');
    }
}
