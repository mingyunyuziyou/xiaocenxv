<?php


namespace app\api\controller\v1;

use app\api\model\Banner as BannerModel;

use app\api\model\Image;
use app\api\validate\BaseValidate;
use app\api\validate\IDMustBePostiveInt;
use app\api\validate\TestValidate;
use app\lib\exception\BannerMissException;
use think\Exception;
use think\Validate;

class Banner
{
    /**
     * 获取指定id的banner信息
     * $url /banner/:id
     * $http GET
     * $id Banner  的id号
     */
    public function getBanner($id)
    {
        (new IDMustBePostiveInt())->goCheck();
        $banner = BannerModel::getBannerByID($id);
//        $banner->hidden(['delete_time', 'update_time']);
        if (!$banner) {
            throw new BannerMissException();
        }
        return $banner;


    }

}
