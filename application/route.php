<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;


// Route: rule（'路由表达式,'路由地址',请求类型','路由参数(数组)','变量短则(数组)' ）

//获取轮播图
Route::get('api/:v/banner/:id', 'api/:v.Banner/getBanner');
//获取轮播主题
Route::get('api/:v/theme', 'api/:v.Theme/getSimpleList');
//获取轮播主题图片id
Route::get('api/:v/theme/:id', 'api/:v.Theme/getComplexOne');


//获取最近新品
Route::get('api/:v/product/recent', 'api/:v.Product/getRecent');
//获取分类商品
Route::get('api/:v/product/by_category', 'api/:v.Product/getAllInCategory');
//获取商品
Route::get("api/:v/product/:id", 'api/:v.Product/getOne', [], ['id' => '\d+']);

////商品路由分组
//Route::group('api/:v/product', function () {
////获取最近新品
//    Route::get('/recent', 'api/:v.Product/getRecent');
////获取分类商品
//    Route::get('/by_category', 'api/:v.Product/getAllInCategory');
////获取商品
//    Route::get("/:id", 'api/:v.Product/getOne', [], ['id' => '\d+']);
//});


//获取全部类目
Route::get('api/:v/category/all', 'api/:v.Category/getAllCategories');


//获取令牌
Route::post('api/:v/token/user', 'api/:v.Token/getToken');


//商品更新
Route::post('api/:v/address', 'api/:v.Address/createOrUpdateAddress');
